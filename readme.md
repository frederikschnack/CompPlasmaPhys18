My Code snippets for the lecture [Computational Plasma Physics](http://www-m16.ma.tum.de/Allgemeines/CompPlasmaPhys18) @ TUM. 
Written exclusively in #julialang (except for cardinal B-Splines called from Python). 

| files        | content           | 
| :-------------: |:-------------: | 
| sheet 1   | 1D poisson solver w/ FD schemes | 
| sheet 2_1      | 1D constant-coeffcient advection w/ FD and spectral schemes    |  
| sheet 2_2 | 1D-1V vlasov-poisson w/ spectral solvers    |  
| sheet 3 | 1D Galerkin FEM w/ Lagrange basis and cardinal B-Splines      |  
| poisson fourier | fourier solver for poisson and constant advection problems      |  


![ ](https://gitlab.lrz.de/uploads/-/system/personal_snippet/327/bc45d71044ed63ca8329c67bee9b7d49/landau_damping_1d1v.gif)